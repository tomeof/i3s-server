const notFound = (err, req, res, next) => {
  res.sendStatus(404);
  res.end();
}

module.exports = notFound;