const config = require('../config');
const fs = require('fs');
const pug = require("pug");

const handle = (req, res) => {

  const directories = fs.readdirSync(`${config.BASE_DIR}/scenelayers`, {
    withFileTypes: true
  }).reduce((a, c) => {
    c.isDirectory() && a.push(c.name)
    return a
  }, []);

  const compiledFunction = pug.compileFile("./views/sceneLayers.pug");
  let sceneLayers = compiledFunction({ layers: directories });
  res.status(200).send(sceneLayers);

};

module.exports = handle;