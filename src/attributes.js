const config = require('../config');
const fs = require('fs');

const handle = (req, res) => {

  let baseUrl = req.baseUrl.replace(/^\/?|\/?$/g, "");
  let resourse = `${config.BASE_DIR}/${baseUrl}/${req.params.nodeid}/${req.params.resource}/${req.params.subresource}/${req.params.subresourceid}${config.FOLDER_INDEX.ATTRIBUTES}`;
  if (!fs.existsSync(resourse)) throw new Error("not found");

  res.set({
    'Content-Disposition': 'attachment; filename=0.bin',
    'Content-Type': 'application/octet-stream',
    'Content-Encoding': 'gzip'
  });
  res.sendFile(resourse);

};

module.exports = handle;