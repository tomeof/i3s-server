const _default_ = require('./default');
const node = require('./node');
const nodepages = require('./nodepages');
const shared = require('./shared');
const features = require('./features');
const geometries = require('./geometries');
const attributes = require('./attributes');
const textures = require('./textures');
const layer = require('./layer');
const layers = require('./layers');

const handlers = {
  _default_: _default_,
  node: node,
  nodepages: nodepages,
  shared: shared,
  features: features,
  geometries: geometries,
  attributes: attributes,
  textures: textures,
  layer: layer,
  layers: layers
}

const getHandler = (token) => {
  return handlers[token] || handlers._default_;
};

module.exports = getHandler;