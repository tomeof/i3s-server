const config = require('../config');
const fs = require('fs');

const handle = (req, res) => {

  let baseUrl = req.baseUrl.replace(/^\/?|\/?$/g, "");
  let resourse = `${config.BASE_DIR}/${baseUrl}/${req.params.pagesid}${config.FOLDER_INDEX.NODEPAGES}`;
  if (!fs.existsSync(resourse)) throw new Error("not found");

  res.set({
    'Content-Type': 'application/json',
    'Content-Encoding': 'gzip'
  });
  res.sendFile(resourse);

};

module.exports = handle;