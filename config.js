module.exports = {
  BASE_DIR: __dirname,
  PORT: 3000,
  FOLDER_INDEX: {
    SCENELAYER: "3dSceneLayer.json.gz",
    NODE: "3dNodeIndexDocument.json.gz",
    NODEPAGES: ".json.gz",
    SHARED: "sharedResource.json.gz",
    FEATURES: ".json.gz",
    GEOMETRIES: ".bin.gz",
    ATTRIBUTES: ".bin.gz",
    TEXTURES: ".bin.dds.gz"
  }
};