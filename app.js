const express = require('express');
const config = require('./config')
const cors = require("./src/cors");
const scenelayers = require("./routes/scenelayers");
const nodes = require("./routes/nodes");
const nodepages = require("./routes/nodepages");
const errorHandler = require("./src/errorHandler");
const port = config.PORT || 9000;

const app = express();

app.get("/favicon.ico", (req, res) => {
  res.sendStatus(204);
  res.end();
});

app.use(cors);
app.use("/scenelayers", scenelayers);
app.use("*nodes", nodes);
app.use("*nodepages", nodepages);
app.use(errorHandler);

app.listen(port, err => {
  if (err) {
    return console.log(err);
  }
});