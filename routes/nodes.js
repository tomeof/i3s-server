const express = require("express");
const getHandler = require('../src/handler');

let router = express.Router();

router.route("/:nodeid").get((req, res) => {

  let handler = getHandler("node");
  handler(req, res);

});

router.route("/:nodeid/:resource").get((req, res) => {

  let param = req.params.resource;
  let handler = getHandler(param);
  handler(req, res);

});

router.route("/:nodeid/:resource/:resourceid").get((req, res) => {

  let param = req.params.resource;
  let handler = getHandler(param);
  handler(req, res);

});

router.route("/:nodeid/:resource/:subresource/:subresourceid").get((req, res) => {

  let param = req.params.resource;
  let handler = getHandler(param);
  handler(req, res);

});

router.route("*").get((req, res) => {

  throw new Error("not found");

});

module.exports = router;