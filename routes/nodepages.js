const express = require("express");
const getHandler = require('../src/handler');

let router = express.Router();

router.route("/:pagesid").get((req, res) => {

  let param = req.params.nodeid;
  let handler = getHandler("nodepages");
  handler(req, res);

});

router.route("*").get((req, res) => {

  throw new Error("not found");

});

module.exports = router;