const express = require("express");
const path = require("path");
const getHandler = require('../src/handler');
const url = require('url');

let router = express.Router();

router.route("/").get((req, res) => {

  if (!req.originalUrl.endsWith("/") && !path.extname(req.originalUrl)) {
    let newUrl = url.format({
      protocol: req.protocol,
      host: req.get("host"),
      pathname: req.originalUrl,
    });
    res.redirect(301, `${newUrl}/`);
    return;
  }

  let handler = getHandler("layers");
  handler(req, res);

});

router.route("/:layer/layers/:layerid").get((req, res) => {

  let handler = getHandler("layer");
  handler(req, res);

});

module.exports = router;